// W05 JUnit: Main
// This code is used to practice UJunit testing by collecting a user's name and age and creating an ID object.
// CIT 360
// Name: Joshua Fullmer
// Oct 13 2021

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static org.junit.Assert.assertThat;

public class Main {
    public static void main(String[] args) {
        String userName = nameCollection();
        String[] splitName = nameSplit(userName);
        int userAge = ageCollection();
        String firstName = storeFirstName(splitName);
        String lastName = storeLastName(splitName);
        System.out.println("Your first name is " + firstName);
        if (lastName == null) {
            System.out.println("You do not have a last name.");
        } else {
            System.out.println("Your last name is " + lastName);
        }
        ID_Card person = new ID_Card(userName, userAge);
        System.out.println();
        System.out.println("Your full name is " + person.getName());
        System.out.println("Your age is " + person.getAge());
        System.out.println("Your ID card has been created and stored.");
        System.out.println("Goodbye.");
    }
    static String nameCollection() {
        Scanner in = new Scanner(System.in);
        String name;
        while(true) {
            try {
                System.out.println("What is your full name?");
                name = in.nextLine();
                break;
            } catch (NoSuchElementException e) {
                System.out.println(e);
                System.out.println("Input was exhausted.");
            }
        }
        return name;
    }
    static int ageCollection() {

        int age;

        while(true){
            try{
                Scanner in = new Scanner(System.in);
                System.out.println("How old are you?");
                age = in.nextInt();
                break;
            } catch (InputMismatchException e) {
                System.out.println("You must enter a whole number.");
            } catch (NoSuchElementException e) {
                System.out.println(e);
                System.out.println("Input was exhausted.");
            }
        }
        return age;
    }
    static String[] nameSplit(String name) {
        String[] nameArray;
        if (name != null) {
            nameArray = name.split("\\s");
            return nameArray;
        } else {
            return null;
        }
    }
    static String storeFirstName(String array[]) {
        return array[0];
    }
    static String storeLastName(String array[]) {
        if (array.length > 1) {
            return array[array.length - 1];
        } else {
            return null;
        }
    }
    static boolean isEven(int age) {
        if (age % 2 == 0) {
            return true;
        }
        else {
            return false;
        }
    }

}
