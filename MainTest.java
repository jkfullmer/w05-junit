
import static org.junit.Assert.*;
import org.junit.Test;

public class MainTest {
    // assertEquals
    @Test
    public void nameTest() {
        String fullName[] = {"Steve", "kip", "Mills"};
        String expected = "Steve";
        String actual = Main.storeFirstName(fullName);
        assertEquals(expected, actual);
    }

    // assertArrayEquals
    @Test
    public void arrayTest() {
        String[] expected = {"this", "is", "me"};
        String[] actual = Main.nameSplit("this is me");
        assertArrayEquals(expected, actual);
    }
    // assertTrue
    @Test
    public void ageIsEvenTrue() {
        int age = 14;
         assertTrue(Main.isEven(age));
    }
    // assertFalse
    @Test
    public void ageIsEvenFalse() {
        int age = 15;
        assertFalse(Main.isEven(age));
    }
    // assertNull

    @Test
    public void nameEmptyTest() {
        String name = null;
        assertNull(Main.nameSplit(name));
    }

    // assertNotNull
    @Test
    public void nameNotEmptyTest() {
        String name = "Bruce Wayne";
        assertNotNull(Main.nameSplit(name));
    }
    // assertSame
    @Test
    public void sameID_Card() {
        ID_Card prophet = new ID_Card("Russell Marion Nelson", 97);
        ID_Card churchPresident = prophet;
        assertSame(prophet, churchPresident);
    }
    // assertNotSame
    @Test
    public void notSameID_Card() {
        ID_Card prophet = new ID_Card("Russell Marion Nelson", 97);
        ID_Card churchPresident = new ID_Card("Russell Marion Nelson", 97);
        assertNotSame(prophet, churchPresident);
    }
    // assertThat
    @Test
    public void ageIsTest() {
        ID_Card person1 = new ID_Card("Buddy", 10);
        String name = person1.getName();
        Assert.assertThat(name, equalTo("Buddy"));
    }
}